<?php
/**
 * Simple custom product
 */
if (!defined('ABSPATH')) {
    exit;
}
global $product;
do_action('gift_card_before_add_to_cart_form');
$smell = get_field('smell');
$strength = get_field('strength');

$get_price = get_post_meta($product->get_id(), '_custom_price');
$price = 0;
?>


<h2 class="custom-nala-heading">Create Your Own</h2>

<form class="gift_card_cart" id="custom_product" method="post" enctype='multipart/form-data'>
    <h3>Scent</h3>
    <section>
        <!--        <legend>Smell</legend>-->
        <?php if (!empty($smell)): ?>
            <ul class="smell-steps ss">
                <?php foreach ($smell as $s): ?>
                    <li>
                        <label for="<?php echo $s['label']; ?>">
                            <img src="<?php echo $s['image']; ?>" alt="smell">
                            <input type="radio" id="<?php echo $s['label']; ?>" class="required" name="smell" value="<?php echo $s['label']; ?>"/>
                            <span class="show-val"><?php echo $s['label']; ?></span>
                        </label>
                    </li>
                <?php endforeach; ?>
            </ul>
            <p class="container-error"></p>
        <?php endif; ?>
    </section>

    <h3>Strength</h3>
    <section>
        <!--        <legend>Strength</legend>-->
        <?php if (!empty($strength)): ?>
            <ul class="strength-steps ss">
                <?php foreach ($strength as $s): ?>
                    <li>
                        <label for="<?php echo $s['label']; ?>">
                            <img src="<?php echo $s['image']; ?>" alt="strength">
                            <input type="radio" id="<?php echo $s['label']; ?>" class="required" name="strength" value="<?php echo $s['label']; ?>"/>
                            <span class="show-val"><?php echo $s['label']; ?></span>
                        </label>
                    </li>
                <?php endforeach; ?>
            </ul>
            <p class="container-error"></p>
        <?php endif; ?>
    </section>

    <h3>myNala</h3>
    <section>
        <!--        <legend>Checkout</legend>-->

        <table id="custom-names">
            <thead>
                <tr>
                    <th><label for="custom_name">Made custom for: </label></th>
                    <th>Quantity</th>
                    <th>Add/Remove</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="text" name="custom_name[0]" id="custom_name" class="required" maxlength="24" placeholder="Enter name" /></td>
                    <td>
                        <div class="quantity">
                            <a class="qty-btn" onclick="this.parentNode.querySelector('input[type=number]').stepDown();"></a>
                            <input type="number" name="quantity_custom[]" id="quantity" value="1" min="1"/>
                            <a class="qty-btn plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp();"></a>
                        </div>
                    </td>
                    <td><span class="add-remove-name"><button id="add-name" class="btn">Add</button></span></td>
                </tr>
            </tbody>
        </table>
        <label for="note">Additional Note: </label>
        <textarea name="note" maxlength="450" id="note" class="add-note-custom" placeholder="Add message"></textarea>
        <!--        <label for="quantity" class="cq">Quantity: </label>-->
        <!--        <div class="quantity">-->
        <!--            <a class="qty-btn" onclick="this.parentNode.querySelector('input[type=number]').stepDown();"></a>-->
        <!--            <input type="number" name="quantity" id="quantity" value="1" min="1"/>-->
        <!--            <a class="qty-btn plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp();"></a>-->
        <!--        </div>-->
    </section>

    <h3>Checkout</h3>
    <section class="custom_product-p-3">
        <p>Scent: <span class="scent-selected"></span></p>
        <p>Strength: <span class="strength-selected"></span></p>
        <div class="names">

        </div>
        <p class="note-review">Note: <span class="note-added"></span></p>
        <button type="submit" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>" class="single_add_to_cart_button button alt custom-add-to-cart-btn"><?php echo esc_html($product->single_add_to_cart_text()); ?></button>

    </section>

</form>

<script>
    jQuery(function ($) {

        // Custom Nala scirpts
        var form = $("#custom_product");

        $("#custom_product").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            enableFinishButton: false,
            enableAllSteps: false,
            onInit: function (event, current) {
                $('.actions > ul > li:first-child a').attr('style', 'display:none'); //removes prev button
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onStepChanged: function (event, current, next) {
                if (current > 0) {
                    $('.actions > ul > li:first-child a').attr('style', '');
                } else {
                    $('.actions > ul > li:first-child a').attr('style', 'display:none');
                }
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
        });

        $("#custom_product").validate({
            errorPlacement: function errorPlacement(error, element) {
                //console.log('EL >', element);
                if (element.is(":radio"))
                {
                    error.appendTo($('.container-error'));
                } else
                { // This is the default behavior
                    error.insertAfter(element);
                }
            },
            /*rules: {
             "smell": {required: true},
             "strength": {required: true},
             "custom_name[*]": {required: true}
             },
             messages: {
             smell: {
             required: 'Choose a smell<br/>'
             },
             strength: {
             required: 'Choose a strength<br/>'
             },
             custom_name: {
             required: 'Enter your name<br/>'
             },
             }*/
            rules: {
                "smell": "required",
                "strength": "required",
                "custom_name[]": "required"
            },
            messages: {
                "smell": "Choose a smell",
                "strength": "Choose a strength",
                "custom_name[]": "Enter your name"
            }
        });


        // adding a custom name to the front of the package
        var inputBox = document.getElementById('custom_name');
        inputBox.onkeyup = function () {
            document.getElementById('show-x-name').innerHTML = inputBox.value;
        };

        var i = 1;

        // review step
        $('.actions li a').on('click', function (e) {
            if ($('#custom_product-p-3').hasClass('current')) {
                $('.names').empty();
                var scent = $('input[name="smell"]:checked').val();
                var strength = $('input[name="strength"]:checked').val();
                var names = $('input[name^="custom_name"]').map(function () {
                    return $(this).val();
                }).get();
                var note = $('textarea[name="note"]').val();
                var quantities = $('input[name="quantity_custom[]"]').map(function () {
                    return $(this).val();
                }).get();
                /*console.log(name);
                 console.log(quantity);*/

                // add values to review
                $('.scent-selected').html(scent);
                $('.strength-selected').html(strength);
                $('.name-added').html(name);
                //$('.quantity-added').html(quantity);


                for (i = 0; i < names.length; i++) {
                    /*console.log('Name: ', names[i]);
                     console.log('Quantity: ', quantities[i]);*/
                    var item = '<p>Name: ' + names[i] + '<br />Quantity: ' + quantities[i] + '</p>';
                    $('.names').append(item);
                }

                // loop through names

                /*$('tr.names-rows').each(function () {
                 $quantities = $('.quantity-names').val();
                 $('.name_list').each(function () {
                 $names = $(this).val();
                 if ($names) {
                 $('.names').append('<p>Name: ' + $names + ' </p><p>Quantity:' + $quantities + ' </p>');
                 }
                 });
                 });*/

                if (!$.trim(note)) {
                    $('.note-review').css('display', 'none');
                } else {
                    $('.note-review').css('display', 'block');
                    $('.note-added').html(note);

                }

                //track and update changes also on button click
                $('.qty-btn').click(function () {
                    var inputVal = document.querySelectorAll('#quantity')[0].value;
                    //$('.quantity-added').html(inputVal);
                });
            }
        });

        $('input').on('change', function () {



        });


        // Add/Remove custom nala names
        // add row and update quantity
        $('#add-name').on('click', function () {
            i++;
            var tableRows = $('#custom-names tr').length;

            if (tableRows <= 5)
                $('#custom-names').append('<tr id="row' + i + '" class="names-rows"><td><input type="text" name="custom_name[' + (tableRows - 1) + ']" placeholder="Enter Name" class="required" /></td><td><div class="quantity">\n' +
                        '            <a class="qty-btn" onclick="this.parentNode.querySelector(\'input[type=number]\').stepDown();"></a>\n' +
                        '            <input type="number" name="quantity_custom[]" id="quantity-' + i + '" class="quantity-names" value="1" min="1"/>\n' +
                        '            <a class="qty-btn plus" onclick="this.parentNode.querySelector(\'input[type=number]\').stepUp();"></a>\n' +
                        '        </div>' +
                        '</td><td><button type="button" name="remove" id="' + i + '" class="btn btn_remove">Remove</button></td></tr>');

            // $('#quantity').val( function(i, oldval) {
            //     return ++oldval;
            // });

            $('input[name^="custom_name"]').each(function () {
                //console.log($(this));
                $(this).rules('add', {
                    required: true
                });
            });

        });

        // remove row and update quantity
        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");

            $('#row' + button_id + '').remove();
            // if($('#quantity').val() > 1) {
            //     $('#quantity').val(function (i, oldval) {
            //         return --oldval;
            //     });
            // }
        });


    });
</script>

<?php do_action('gift_card_after_add_to_cart_form'); ?>