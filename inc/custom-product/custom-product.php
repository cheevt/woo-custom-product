<?php
/**
 * Register custom product type 'custom'
 */
add_action('init', 'register_custom_product_type');

function register_custom_product_type() {

    class WC_Product_package extends WC_Product {

        public $product_type = 'custom';

        public function __construct($product) {
            parent::__construct($product);
        }

    }

}

/**
 *
 */
add_filter('product_type_selector', 'add_custom_package_product');

function add_custom_package_product($types) {
// Key should be exactly the same as in the class
    $types['custom'] = __('Custom');
    return $types;
}

/**
 *
 */
add_filter('woocommerce_product_class', 'woocommerce_product_class', 10, 2);

function woocommerce_product_class($classname, $product_type) {

    if ($product_type == 'custom') { // notice the checking here.
        $classname = 'WC_Product_package';
    }

    return $classname;
}

/**
 * Custom product tab
 */
add_filter('woocommerce_product_data_tabs', 'custom_tab');

function custom_tab($tabs) {

// Key should be exactly the same as in the class product_type
    /* $tabs['custom'] = array(
      'label' => __('Custom', 'wcpt'),
      'target' => 'custom_options',
      'class' => ('show_if_custom'),
      ); */

    //$tabs['general']['class'][] = 'show_if_custom';
    $tabs['inventory']['class'][] = 'show_if_custom';

    return $tabs;
}

/**
 * Custom product type options
 */
add_action('woocommerce_product_data_panels', 'wcpt_custom_options_product_tab_content');

function wcpt_custom_options_product_tab_content() {
// Dont forget to change the id in the div with your target of your product tab
    ?><div id='custom_options' class='panel woocommerce_options_panel'><?php
    ?><div class='options_group'><?php
            woocommerce_wp_checkbox(array(
                'id' => '_enable_custom',
                'label' => __('Enable Custom Product', 'wcpt'),
            ));
            woocommerce_wp_text_input(array(
                'id' => '_custom_price',
                'label' => __('Price', 'wcpt'),
                'placeholder' => '',
                'desc_tip' => 'true',
                'description' => __('Enter Custom Price.', 'wcpt'),
            ));
            ?></div>
    </div><?php
}

/**
 * Save custom options field
 */
add_action('woocommerce_process_product_meta', 'save_custom_options_field');

function save_custom_options_field($post_id) {
    $enable_custom = isset($_POST['_enable_custom']) ? 'yes' : 'no';
    update_post_meta($post_id, '_enable_custom', $enable_custom);
    if (isset($_POST['_custom_price'])) :
        update_post_meta($post_id, '_custom_price', sanitize_text_field($_POST['_custom_price']));
    endif;
}

/**
 * Load custom template for 'custom' product type
 */
add_action('woocommerce_single_product_summary', 'custom_template', 60);

function custom_template() {
    global $product;
//var_dump($product->get_type());
    if ('custom' == $product->get_type()) {
        $template_path = plugin_dir_path(__FILE__) . 'templates/';
// Load the template
        wc_get_template('single-product/custom.php', '', '', trailingslashit($template_path));
    }
}

/**
 * Add data to cart item
 */
add_filter('woocommerce_add_cart_item_data', 'add_cart_item_data', 10, 3);

function add_cart_item_data($cart_item_data, $product_id) {
    //var_dump($_POST);
// Has our option been selected?
    if (isset($_POST['smell']) && isset($_POST['strength'])) {
        $custom_data = array();
        $product = wc_get_product($product_id);
        $price = $product->get_price();
        $get_price = get_post_meta($product_id, '_custom_price');
        if (isset($get_price[0])) {
            $price = $get_price[0];
            $cart_item_data['custom_price'] = $price;
        }
// Store the overall price for the product, including the cost of the warranty
//$cart_item_data['custom_nala_desc'] = 'Smell: ' . $_POST['smell'] . '; Strength: ' . $_POST['strenth'] . '; Note: ' . $_POST['note'];
//$cart_item_data['custom_nala_desc'] = 'Smell: ' . $_POST['smell'] . '; Strength: ' . $_POST['strength'] . '; Note: ' . $_POST['note'];
        $custom_data['smell'] = isset($_POST['smell']) ? $_POST['smell'] : '';
        $custom_data['strength'] = isset($_POST['strength']) ? $_POST['strength'] : '';
        $custom_data['note'] = isset($_POST['note']) ? $_POST['note'] : '';
        if (isset($_POST['custom_name'])) {
            foreach ($_POST['custom_name'] as $name) {
                $custom_data['custom_name'][] = $name;
            }
        }
        if (isset($_POST['quantity_custom'])) {
            foreach ($_POST['quantity_custom'] as $quantity) {
                $custom_data['quantity'][] = $quantity;
            }
        }

        $cart_item_data['custom_data'] = $custom_data;
    }
    return $cart_item_data;
}

/**
 * Display custom data on cart and checkout page.
 */
add_filter('woocommerce_get_item_data', 'get_item_data', 25, 2);

function get_item_data($other_data, $cart_item) {
    if (isset($cart_item ['custom_data'])) {
        $custom_data = $cart_item ['custom_data'];
        //var_dump($custom_data);

        $other_data[] = array('name' => 'Smell',
            'display' => $custom_data['smell']);
        $other_data[] = array('name' => 'Strength',
            'display' => $custom_data['strength']);

        if ($custom_data['custom_name']) {
            $i = 0;
            foreach ($custom_data['custom_name'] as $name) {
                $other_data[] = array('name' => 'Made custom for',
                    'display' => $name);
                $other_data[] = array('name' => 'Quantity',
                    'display' => $custom_data['quantity'][$i]);
                $i++;
            }
        }


        if ($custom_data['note'] != '') {
            $other_data[] = array('name' => 'Note',
                'display' => $custom_data['note']);
        }
    }

    return $other_data;
}

/**
 * Add order item meta.
 */
add_action('woocommerce_add_order_item_meta', 'add_order_item_meta', 10, 2);

function add_order_item_meta($item_id, $values) {
    if (isset($values['custom_data'])) {
        $custom_data = $values['custom_data'];
        wc_add_order_item_meta($item_id, 'Smell', $custom_data['smell']);
        wc_add_order_item_meta($item_id, 'Strength', $custom_data['strength']);
        if ($custom_data['custom_name']) {
            foreach ($custom_data['custom_name'] as $key => $name) {
                wc_add_order_item_meta($item_id, 'Made custom for', $name);
                wc_add_order_item_meta($item_id, 'Quantity', $custom_data['quantity'][$key]);
            }
        }

        if ($custom_data['note'] != '') {
            wc_add_order_item_meta($item_id, 'Note', $custom_data['note']);
        }
    }
}

/**
 * Set custom price and title of product
 */
add_action('woocommerce_before_calculate_totals', 'before_calculate_totals', 10, 1);

function before_calculate_totals($cart_obj) {
    if (is_admin() && !defined('DOING_AJAX')) {
        return;
    }
    //var_dump(WC()->cart->get_cart());
// Iterate through each cart item
    foreach ($cart_obj->get_cart() as $key => $value) {
        //var_dump($value);
        if (isset($value['custom_data']['quantity'])) {
            //$price = $value['custom_price'];
            $qnty = 0;
            foreach ($value['custom_data']['quantity'] as $quantity) {
                $qnty += $quantity;
            }
            $custom_count = count($value['custom_data']['quantity']);
            //var_dump($cart_obj->cart_contents[$key]['quantity']);
            $cart_obj->cart_contents[$key]['quantity'] = $qnty;
            //WC()->cart->set_quantity($qnty);
            //$name = $value['data']->name . ' - Citrus';
            //var_dump($value['data']->get_price());
            if ($custom_count >= 3) {
                $value['data']->set_price($value['data']->get_price() * 0.9);
            }
            //$value['data']->set_price(10);
            /* $description = 'Sve najbolje zelim, bruka miris, jacina top!';
              $value['data']->set_description($description); */
        }
    }
}

//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

/**
 * Exclude products from a particular category on the shop page
 */
add_action('woocommerce_product_query', 'custom_pre_get_posts_query');

function custom_pre_get_posts_query($q) {
    $tax_query = (array) $q->get('tax_query');

    $tax_query[] = array(
        'taxonomy' => 'product_cat',
        'field' => 'slug',
        'terms' => array('custom'), // Don't display products in the clothing category on the shop page.
        'operator' => 'NOT IN'
    );


    $q->set('tax_query', $tax_query);
}

/**
 * Show pricing fields and tax for custom product.
 */
add_action('admin_footer', 'custom_product_js');

function custom_product_js() {
    if ('product' != get_post_type()) :
        return;
    endif;
    ?><script type='text/javascript'>
        jQuery('.options_group.pricing').addClass('show_if_custom').show();
        jQuery('._tax_status_field').parent('div').addClass('show_if_custom').show();
    </script><?php
}

/**
 * Enqueue scripts and styles.
 */
add_action('wp_enqueue_scripts', 'custom_product_scripts');

function custom_product_scripts() {
    wp_enqueue_script('nikolab-steps', get_template_directory_uri() . '/inc/custom-product/js/jquery.steps.min.js', array('jquery'), '', true);
    wp_enqueue_script('nikolab-validate', get_template_directory_uri() . '/inc/custom-product/js/jquery.validate.js', array('jquery'), '', true);

    wp_enqueue_style('steps-css', get_template_directory_uri() . '/inc/custom-product/css/jquery.steps.css', array(), '1.0.0', true);
}

function quantity_update($product_quantity, $cart_item_key, $cart_item) {
    $product_id = $cart_item['product_id'];
    $_product = wc_get_product($product_id);
    // whatever logic you want to determine whether or not to alter the input
    if ($_product->is_type('custom')) {
        return '<h5>' . $cart_item['quantity'] . '</h5>';
    }

    return $product_quantity;
}

add_filter('woocommerce_cart_item_quantity', 'quantity_update', 10, 3);

